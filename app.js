const path = require('path');
const express = require('express');
const OS = require('os');
var bodyParser = require('body-parser');
const mongoose = require("mongoose");
const app = express();
app.use(bodyParser.json());

mongoose.connect("mongodb+srv://superuser:SuperPassword@supercluster.d83jj.mongodb.net/superData?retryWrites=true&amp;w=majority", { useNewUrlParser: true,  useUnifiedTopology: true  }, function(err) {
    if (err) {
     console.log("error!! " + err)
    } else {
     console.log("Connection Successful")
    }
   })
app.use(express.static(path.join(__dirname, '/')));

var Schema = mongoose.Schema;

var dataSchema = new Schema({
 name: String,
 id: Number
});
var userModel = mongoose.model('planets', dataSchema);

app.get('/os', function(req,res){
    console.log("OS "+OS.hostname())
    res.setHeader('Content-Type', 'application/json');
    res.send({"os":OS.hostname()});     
}) 

app.get('/test', function(req,res){
    res.setHeader('Content-Type', 'application/json');
    res.send({"name":"sid"});     
}) 

app.post('/user', function(req,res){
    console.log("name is "+req.body.id)
    userModel.findOne({id:req.body.id}, function(err, browserName){
        if(err){
            console.log("some err");
            res.send("Error in specificbrowser")
        }else{
            console.log("result issssssssssssssss "+browserName );
            res.send(browserName);
        }
    })
}) 

app.get('/', async(req, res) => {
    res.sendFile(path.join(__dirname, '/', 'index.html'));
});



app.listen(3000, () => {
    console.log("Server successfully running on port 3000");
  })
  module.exports = app;