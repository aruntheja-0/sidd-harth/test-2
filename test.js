let server = require("./app");
let chai = require("chai");
let chaiHttp = require("chai-http");

// Assertion 
chai.should();
chai.use(chaiHttp); 

describe('Task APIs', () => {

    describe("Test GET route /test", () => {
        it("It should return all tasksxxxxxxxx", (done) => {
            chai.request(server)
                .get("/test")
                .end((err, response) => {
                    response.should.have.status(200);
                   // response.body.should.be.a('array');
                    response.body.name.should.be.eq('sid');
                done();
                });
        });
    });
});